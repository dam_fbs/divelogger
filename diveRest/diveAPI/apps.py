from django.apps import AppConfig


class DiveapiConfig(AppConfig):
    name = 'diveAPI'
