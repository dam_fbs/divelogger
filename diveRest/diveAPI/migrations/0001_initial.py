# Generated by Django 3.1.2 on 2020-11-01 23:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Diver',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('prenom', models.CharField(max_length=200)),
                ('nom', models.CharField(max_length=200)),
                ('niveau', models.CharField(max_length=200)),
                ('password', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Dive',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('length', models.IntegerField(default=0)),
                ('depth', models.IntegerField(default=0)),
                ('location', models.CharField(max_length=200)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('summary', models.CharField(default='', max_length=200)),
                ('diver', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='diveAPI.diver')),
            ],
        ),
    ]
