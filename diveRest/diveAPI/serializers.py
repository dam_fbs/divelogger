# # serializers.py
from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField

from .models import Dive, Diver

#
class DiverSimpleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Diver
        fields = ('id', 'nom', 'prenom', 'email', 'niveau')


class DiveSerializer(serializers.HyperlinkedModelSerializer):
     diverPrenom = serializers.ReadOnlyField(source='diver.prenom')
     class Meta:
         model = Dive
         fields = ('id', 'length', 'depth', 'location', 'date', 'summary', 'diver', 'diverPrenom')



class DiverSerializer(serializers.HyperlinkedModelSerializer):
    plongee = DiveSerializer(read_only=True, many=True)
    class Meta:
        model = Diver
        fields = ('id', 'nom', 'prenom', 'email', 'niveau', 'password', 'plongee')


