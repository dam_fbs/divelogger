from django.db import models

# Create your models here.

# modele plongeur
class Diver(models.Model):
    id = models.AutoField(primary_key=True)
    prenom = models.CharField(max_length=200)
    nom = models.CharField(max_length=200)
    email = models.CharField(max_length=200, default='')
    niveau = models.CharField(max_length=200)
    password = models.CharField(max_length=200)


    # surcharge de la methode ~tostring
    def __str__(self):
        return self.prenom


#mode plongée avec lien vers le plongeur. ( plongées ManyToOne plongeur )
class Dive(models.Model):
    id = models.AutoField(primary_key=True)
    length = models.IntegerField(default=0)
    depth = models.IntegerField(default=0)
    location = models.CharField(max_length=200)
    date = models.DateField(blank=True)
    summary = models.CharField(max_length=200, default='')
    diver = models.ForeignKey(Diver, on_delete=models.CASCADE, related_name='plongee')

    # surcharge de la methode ~tostring
    def __str__(self):
        return self.summary


