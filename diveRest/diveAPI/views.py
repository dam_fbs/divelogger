from requests import Response
from rest_framework import viewsets
from .serializers import DiveSerializer, DiverSerializer
from .models import Dive, Diver
from rest_framework.decorators import action
#
# # Create your views here.
#


class DiveViewSet(viewsets.ModelViewSet):
     queryset = Dive.objects.all().order_by('-date')
     serializer_class = DiveSerializer


class DiverViewSet(viewsets.ModelViewSet):
     queryset = Diver.objects.all()
     serializer_class = DiverSerializer

     @action(detail=True)
     def test(self, request, pk=None):
          print('ACTION')
          diver = self.get_object()  # retrieve an object by pk provided
          print(diver.prenom)
          dives = Dive.objects.filter(diver=diver).distinct()
          for dive in dives:
               print(dive.id)
          schedule_json = DiveSerializer(dives, many=True, context={'request': request})
          print(schedule_json.data)
          return Response(schedule_json.data)
