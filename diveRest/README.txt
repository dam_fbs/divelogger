API Rest utilisant le django-rest-framework

Modele de données :
Pour l'instant on ne gere pas le Many To Many, on part du principe que :
Un plongeur peut plonger plusieurs fois.
DIVER oneToMany DIVE
Voir la base db.sqlite3 contenue dans ce dossier.

Pour lancer : python3 manage.py runserver 7000
Ecoute sur le port 7000

CONSOLE ADMIN : http://127.0.0.1:7000
Liste des plongées : "dives": "http://127.0.0.1:7000/dives/",
Liste des plongeurs : "divers": "http://127.0.0.1:7000/divers/"


