from django.http import HttpResponseRedirect
from django.shortcuts import render
import requests
import json
from . import forms
from datetime import date, timedelta, datetime


# Create your views here.
from django.core import serializers


# controller de la page d'accueil de l'appli
def home(request):
    #TODO mettre en statique dans fichier de config
    #récuperation des plongées
    r = requests.get('http://localhost:7000/dives/', params=request.GET)

    #récupération du prenom de l'utilisateur ans la session pour affichage dashboard
    username = request.session['divername']
    plongees = json.loads(r.content)
    context = {'plongees': plongees, 'username': username}
    return render(request, 'divelogger/home.html', context)


# controller de la page d'accueil de l'appli
# Page simpliste avec affichage des données du user en base de données
def profil(request):
    userid = request.session.get('diverid')
    r = requests.get('http://localhost:7000/divers/'+str(userid), params=request.GET)
    diver = json.loads(r.content)
    context = {'diver': diver}
    return render(request, 'divelogger/profil.html', context)


# controller de la page du carnet de plongées
def carnet(request):
    formND = forms.NewDive()
    # Formulaire soumis -> nouvelle inscription
    if request.method == 'POST':
        # Récupération des données du formulaire d'inscritopn
        formDive = forms.NewDive(request.POST)
        if formDive.is_valid():
            # Préparation de la requête d'inscription
            # mettre données statiques dans fichier de config
            url = "http://localhost:7000/dives/"
            header = {"content-type": "application/json"}
            diverUrl = 'http://localhost:7000/divers/'+str(request.session.get('diverid'))+'/'
            data = {'length': formDive.cleaned_data['length'],
                    'depth': formDive.cleaned_data['depth'],
                    'location': formDive.cleaned_data['location'],
                    'date': str(formDive.cleaned_data['date']),
                    'summary': formDive.cleaned_data['summary'],
                    'diver': diverUrl}

            # Appel à l'API Rest pour ajouter une plongée pour cet utilisateur
            response = requests.post(url, data=json.dumps(data), headers=header)
            return HttpResponseRedirect('/carnet')
        else:
            initCarnet(formND, request)
            # for field in formDive:
            #     for error in field.errors:
            #         #log de l'erreur dans le formulaire ( utile pour la date )
            #         print(error)
    else:
        return initCarnet(formND, request)
    return render(request, 'divelogger/carnet.html', {'formND': formND})


# récupération des statistiques des plongées de l'utilisateur
def getStatistiques(plongees):
    saturationLimit = datetime.today() - timedelta(days=2)
    profMax = 0
    dureeMax = 0
    saturation = False
    for plongee in plongees:
        datePlongee = datetime.strptime(plongee['date'], '%Y-%m-%d')
        if datePlongee > saturationLimit:
            saturation = True
        if (plongee['length'] > dureeMax):
            dureeMax = plongee['length']
        if (plongee['depth'] > profMax):
            profMax = plongee['depth']
    return profMax, dureeMax, saturation

# Page de carnet formulaire vierge avec infos user
def initCarnet(formND, request):

    userid = request.session.get('diverid')
    username = request.session.get('divername')

    r = requests.get('http://localhost:7000/divers/' + str(userid), params=request.GET)
    diver = json.loads(r.content)
    plongees = diver['plongee']

    totalDives = len(plongees)
    profMax, dureeMax, saturation = getStatistiques(plongees)

    context = {'plongees': plongees,
               'username': username,
               'formND': formND,
               'totalDives': totalDives,
               'profMax': profMax,
               'dureeMax': dureeMax,
               'saturation': saturation}

    return render(request, 'divelogger/carnet.html', context)