from django import forms

class NewDive(forms.Form):
    length = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'durée(mn'}))
    depth = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'profondeur(m'}))
    location = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Lieu'}))
    date = forms.DateField(widget=forms.DateInput(attrs={'class': 'form-control', 'placeholder': 'aaaa-MM-dd'}))
    summary = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Résumé'}))

    class Meta:
        #model = SignUp
        fields = ['length', 'depth', 'location', 'date', 'summary']
