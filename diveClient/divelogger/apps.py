from django.apps import AppConfig


class DiveloggerConfig(AppConfig):
    name = 'divelogger'
