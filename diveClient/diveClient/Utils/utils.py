import json
from authent.models import Diver

class JSONDiverEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, Diver):
            return str(o)
        return json.JSONEncoder.default(self, o)
