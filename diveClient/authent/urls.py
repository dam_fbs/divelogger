from django.urls import path
from . import views
from .views import regform, accueil

urlpatterns = [
    path('', accueil, name='accueilForm'),
    path('regform', regform, name='accueilForm')
]