from django import forms

# formulaire pour l'inscription
class Register(forms.Form):
    prenom = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Prenom'}))
    nom = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nom'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Email'}))
    niveau = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Niveau'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'}))
    confirmation = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Confirmation'}))

    class Meta:
        #model = SignUp
        fields = ['prenom', 'nom', 'email', 'niveau', 'password', 'confirmation']


# formulaire pour la connection
class SignUp(forms.Form):
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Email'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'}))

    class Meta:
        #model = SignUp
        fields = ['username', 'password']