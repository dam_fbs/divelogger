from django.shortcuts import render, HttpResponseRedirect, redirect
import requests
import json
from . import forms
from django.core.cache import cache


# controller du formulaire d'enregistrement
def regform(request):
    html = ''
    # Si le formulaire a été soumis
    if request.method == 'POST':
        # Récupération des données du formulaire d'enregistrement
        formReg = forms.Register(request.POST)
        # Verification de la validité du formulaire
        if formReg.is_valid():
            # Préparation de la requête d'inscription
            response = InscrireDiver(formReg)
            print('REPONSE : ', response.status_code)
            if response.status_code == 200 or response.status_code == 201:
                return initSessionUserAndGo(request, response.json().get('prenom'), response.json().get('id'))
                # routage vers la page d'accueil de l'application

            else:
                form = forms.Register()
                html = 'Erreur d\'enregistrement, veuillez verifier vos informations'
                render(request, 'accueil.html', {'html': html, 'form': form})
    else:
        form = forms.Register()
        return render(request, 'accueil.html', {'html': html, 'form': form})


# methode pour garder en memoire session le nom et l'id du plongeur
def initSessionUserAndGo(request, divername, diverId):
    request.session['divername'] = divername
    request.session['diverid'] = diverId
    return HttpResponseRedirect('/divelogger')


# Récuperation des données formulaire et envoi JSON vers l'API Rest
def InscrireDiver(formReg):
    #TODO Mettre l'URL dans le fichier de config
    url = "http://localhost:7000/divers/"
    header = {"content-type": "application/json"}
    data = {'nom': formReg.cleaned_data['nom'],
            'prenom': formReg.cleaned_data['prenom'],
            'email': formReg.cleaned_data['email'],
            'niveau': formReg.cleaned_data['niveau'],
            'password': formReg.cleaned_data['password']}
    #print('data:', data)
    response = requests.post(url, data=json.dumps(data), headers=header)
    return response


# Controller du formulaire d'authentification
def accueil(request):
    html = ''
    # tentative de connection
    if request.method == 'POST':
        formSignup = forms.SignUp(request.POST)
        if formSignup.is_valid():
            # recupération des utilisateurs
            #TODO : url en statique fichier de config
            r = requests.get('http://localhost:7000/divers/', params=request.GET)
            divers = json.loads(r.content)
            for diver in divers:
                if diver['email'] == formSignup.cleaned_data['email'] and diver['password'] == formSignup.cleaned_data['password']:
                    return initSessionUserAndGo(request, diver['prenom'], diver['id'])
            else:
                #pas de user
                return demandeConnection(request, True)
        else:
            #formulaire invalide
            return demandeConnection(request, True)
    else:
        # formulaire vierge
        return demandeConnection(request, False)


# methode qui gère l'affichage du formulaire vierge et gere le cas d'une connection échouée
def demandeConnection(request, erreur):
    if erreur:
        html = 'Mauvais identifiants, Veuillez réessayer'
    else:
        html = ''
    form = forms.SignUp()
    formReg = forms.Register()
    return render(request, 'accueil.html', {'form': form, 'html': html, 'formreg': formReg})